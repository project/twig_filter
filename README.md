# Twig Filter

[Twig][1] is a very powerful text filtering system which has features not available in current drupal text filting systems. Twig gives the power of a basic programming language with conditional blocks and looping and well as many other features.

## Installation
1. Install this module as normally would be done in Drupal. See [Installing modules and themes][2] for more information.
2. Download the Twig library from the [Twig][1] home page.
3. Unpack the library into the sites/all/libraries/twig directory so the AutoLoader.php is in the page sites/all/libraries/twig/lib/Twig/AutoLoader.php

## Using Twig filter
### Rules
To your rules add the Trig Filter to your rules. Add the template text to the Action and set the variable to be provided. Then in the next action you will be able to pass the variable created to any proceeding action.

### Caching
Twig has a very comprehensive caching system which can dramitcally increase the speed of processing. To enable this add the following to your settings.php

`$conf[‘twig_cache_dir’] = ‘path to cache dir’;`

## Patches
With Rules all entities are wrapped with the entity wrapper and to work with Twig the ::__toString() function is not returning the current function. See [\#1991660](http://drupal.org/node/1991660) which will allow the Drupal objects to be accesses correctly.

## Twig Syntax
See the Documentation page on the [Twig][1] site

[1]: http://twig.sensiolabs.org/
[2]: http://drupal.org/documentation/install/modules-themes