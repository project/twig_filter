<?php
/**
 * @file
 * Token engine for Custom Formatters modules.
 */

/**
 * Implements twig_filter_engine_hook_theme_alter().
 */
function twig_filter_engine_twig_theme_alter(&$theme) {
  $theme['twig_filter_engine_php_export'] = array(
    'variables' => array(
      'item' => NULL,
      'module' => NULL,
    ),
    'template' => 'twig_filter.export',
    'path' => drupal_get_path('module', 'twig_filter') . '/custom_formatters',
  );
}

/**
 * Settings form callback for Custom Formatters Token engine.
 */
function twig_filter_engine_twig_settings_form(&$form, $form_state, $item) {
  $form['code']['#attributes']['class'][] = 'syntax-html';

  // Additional debugging modes.
  $form['preview']['options']['dpm'] = array(
    '#type' => 'container',
  );
  $form['preview']['options']['dpm']['vars'] = array(
    '#type' => 'checkbox',
    '#title' => t('Output variables available (requires <a href="http://drupal.org/project/devel">Devel</a> module).'),
    '#default_value' => module_exists('devel'),
    '#disabled' => !module_exists('devel'),
  );
  $form['preview']['options']['dpm']['html'] = array(
    '#type' => 'checkbox',
    '#title' => t('Output raw HTML (requires !devel module).', array('!devel' => l(t('Devel'), 'http://drupal.org/project/devel'))),
    '#default_value' => module_exists('devel'),
    '#disabled' => !module_exists('devel'),
  );
}

/**
 * Render callback for Custom Formatters Token engine.
 */
function twig_filter_engine_twig_render($formatter, $obj_type, $object, $field, $instance, $langcode, $items, $display) {
  $output = '';

  $variables = array();
  $variables['site'] = entity_metadata_site_wrapper(); // Add in the site information as it is always handy
  $variables[$obj_type] = entity_metadata_wrapper($obj_type, $object);
  $variables['items'] = $variables[$obj_type]->{$field['field_name']};

  if (isset($formatter->preview)) {
    $debug = TRUE;
  }
  else {
    $debug = FALSE;
  }

  if ($twig = twig_filter_setup_environment($debug)) {
    $output = $twig->render($formatter->code, $variables);
  }
  else {
    drupal_set_message(t('Twig library not loaded.'), 'error');
  }

  // Preview debugging; Show the available variables data.
  if (module_exists('devel') && isset($formatter->preview) && $formatter->preview['options']['dpm']['vars']) {
    $raw = array();

    foreach ($variables as $key => $value) {
      $raw[$key] = $value->raw();
    }
    $raw['items'] = $items;
    dpm($raw);
  }

  return $output;
}

/**
 * Export callback for Custom Formatters Token engine.
 */
function twig_filter_engine_twig_export($item, $module) {
  return theme('twig_filter_token_export', array('item' => $item, 'module' => $module));
}

/**
 * Help callback for Custom Formatters Token engine.
 */
function twig_filter_engine_twig_help() {
  return t('A HTML + Token Formatter utilizes Drupal Tokens and the Token module to create easy yet powerful formatters.') . "<br />\n"
       . t('All available Tokens can be navigated via the Tokens fieldset displayed below the Formatter field.');
}
