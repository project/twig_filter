<?php

/**
 * Implements hook_rules_action_info().
 */
function twig_filter_rules_action_info() {
  $library = libraries_detect('twig');
  if ($library['installed']) {
    return array(
      'twig_filter' => array(
        'base' => 'twig_filter_rules_filter',
        'label' => t('Twig filter'),
        'group' => t('System'),
        'parameter' => array(
          'value' => array(
            'type' => 'text',
            'label' => t('Template')
          ),
        ),
        'provides' => array(
          'filtered_text' => array(
            'type' => 'text',
            'label' => t('Filtered Text'),
          ),
        ),
      ),
    );
  }
}

/**
 * Rules action for debugging values.
 */
function twig_filter_rules_filter($template, $variables, $state) {
  if ($twig = twig_filter_setup_environment()) {
    return array('filtered_text' => $twig->render($template, $state->variables));
  }
  else {
    drupal_set_message(t('Twig library not loaded.'), 'error');
  }
}
